import Loader.HTMLDownloader;
import Loader.XMLDownloader;
import Parsers.StrParser;
import Storages.CurrencyStorage;
import Storages.HTMLStorage;
import Storages.XMLStorage;

public class Runner {

    public static void main(String args[]){
        CurrencyStorage storage = new CurrencyStorage();
        storage.fillFromHTMLStorage("USD");
        System.out.println(storage.prognosesForTomorrow());
    }
}
