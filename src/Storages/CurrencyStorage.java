package Storages;

import Parsers.StrParser;
import Parsers.XMLParser;

import java.util.ArrayList;

public class CurrencyStorage {

    private ArrayList<ValuteObject> valuteObjects;

    public CurrencyStorage(){
        valuteObjects = new ArrayList<ValuteObject>();
    }

    public CurrencyStorage(ArrayList<ValuteObject> o){
        valuteObjects = o;
    }

    public ValuteObject getValuteObject(int i){return valuteObjects.get(i);}

    public ArrayList<ValuteObject> getValuteObjects(){return valuteObjects;}

    public void addValuteObject(ValuteObject o){
        valuteObjects.add(o);
    }

    public void fillFromHTMLStorage(String abbreviation){
        HTMLStorage storage = new HTMLStorage();
        storage.fillStorage();
        ArrayList<String> pages = storage.getHTMLPages();
        for (String page : pages){
            StrParser.parse(page);
            addValuteObject(StrParser.generateValuteObject(abbreviation));
        }
    }

    public void fillFromXMLStorage(String abbreviation){
        XMLStorage storage = new XMLStorage();
        storage.fillStorage();
        ArrayList<String> pages = storage.getXMLPages();
        for (String page : pages){
            XMLParser.parse(page);
            addValuteObject(XMLParser.generateValuteObject(abbreviation));
        }
    }

    public double prognosesForTomorrow(){  //Сверял результат с прогнозом на сайте нац. банка и они сошлись
        int h = 1;
        double yi[] = new  double[7];
        double dyi[] = new double[6];
        double d2yi[] = new double[5];
        double d3yi[] = new double[4];
        for (int i = 0, j = 6; i<7; i++, j--){
            yi[i] = valuteObjects.get(j).getRate();
        }
        for (int i = 0; i<6; i++){
            dyi[i] = yi[i+1] - yi[i];
        }
        for (int i = 0; i<5; i++){
            d2yi[i] = dyi[i+1] - dyi[i];
        }
        for (int i = 0; i<4; i++){
            d3yi[i] = d2yi[i+1] - d2yi[i];
        }
        return (yi[0] + dyi[0] + d2yi[0]/2 + d3yi[0]/6);
    }
}
