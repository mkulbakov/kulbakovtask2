package Storages;

import Loader.Threads.XMLLoadThread;

import java.util.ArrayList;

public class XMLStorage implements Storage {

    private ArrayList<String> xmlPages;
    private static ArrayList<String> pagesPool;
    private static int threadsWorks;
    private static int threadsCompleted;

    public XMLStorage(){
        xmlPages = new ArrayList<String>();
        pagesPool = new ArrayList<String>();
        threadsWorks = 0;
        threadsCompleted = 0;
    }

    public static void clearThePool(){
        pagesPool = new ArrayList<String>();
        threadsWorks = 0;
        threadsCompleted = 0;
    }

    public static int getThreadsWorks(){return threadsWorks;}

    public static void incThreadsWorks(){threadsWorks++;}

    public static void incThreadsCompletes(){threadsCompleted++;}

    public ArrayList<String> getXMLPages(){return xmlPages;}

    public String getXMLPage(int i){return xmlPages.get(i);}

    public void addXMLPage(String page) {xmlPages.add(page);}

    public static void addPageToPool(String page){
        pagesPool.add(page);
    }

    public void importPool(){
        for (String page : pagesPool)
            addXMLPage(page);
    }

    public void fillStorage(){
        clearThePool();
        Thread loadThread = new Thread(new XMLLoadThread());
        for (int i = 0; i < daysCount; i++)
            loadThread.run();
        importPool();
        if (threadsCompleted != threadsWorks) System.out.println("Warning : Not all pages was loaded.");
        clearThePool();
    }
}