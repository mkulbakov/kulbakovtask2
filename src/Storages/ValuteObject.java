package Storages;

public class ValuteObject {

    private String abbreviation;
    private int scale;
    private double rate;
    private String date;

    public ValuteObject(){
        this("",0,0,"");
    }

    public ValuteObject(String a, int s, double r, String d){
        abbreviation = a;
        scale = s;
        rate = r;
        date = d;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public int getScale() {
        return scale;
    }

    public double getRate() {
        return rate;
    }

    public String getDate() {
        return date;
    }

    public String toString() {
        return abbreviation+";"+scale+";"+rate+";"+date;
    }
}
