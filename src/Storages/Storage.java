package Storages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public interface Storage {

    final static int daysCount = 30;
    final static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    public default int getDaysCount(){return daysCount;}
    public default SimpleDateFormat getDateFormat(){return dateFormat;}

    public void fillStorage();
}
