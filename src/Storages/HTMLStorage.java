package Storages;

import Loader.Threads.HTMLLoadThread;

import java.util.ArrayList;

public class HTMLStorage implements Storage{

    private ArrayList<String> htmlPages;
    private static ArrayList<String> pagesPool;
    private static int threadsWorks;
    private static int threadsCompleted;

    public HTMLStorage(){
        htmlPages = new ArrayList<String>();
        pagesPool = new ArrayList<String>();
        threadsWorks = 0;
        threadsCompleted = 0;

    }

    public static void clearThePool(){
        pagesPool = new ArrayList<String>();
        threadsWorks = 0;
        threadsCompleted = 0;
    }

    public static void incThreadsWorks(){threadsWorks++;}

    public static void incThreadsCompleted(){threadsCompleted++;}

    public static int getThreadsWorks(){return threadsWorks;}

    public ArrayList<String> getHTMLPages(){return htmlPages;}

    public String getHTMLPage(int i){return htmlPages.get(i);}

    public void addHTMLPage(String page) {htmlPages.add(page);}

    public static void addPageToPool(String page){
        pagesPool.add(page);
    }

    public void importPool(){
        for (String page : pagesPool)
            addHTMLPage(page);
    }

    public void fillStorage(){
        clearThePool();
        Thread loadThread = new Thread(new HTMLLoadThread());
        for (int i = 0; i < daysCount; i++)
            loadThread.run();
        importPool();
        if (threadsCompleted != threadsWorks) System.out.println("Warning : Not all pages was loaded.");
        clearThePool();
    }
}