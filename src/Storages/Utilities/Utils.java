package Storages.Utilities;

import java.util.Calendar;
import java.util.Date;

public class Utils {

    public static Date generateDate(int n){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -n);
        return new Date(calendar.getTimeInMillis());
    }
}
