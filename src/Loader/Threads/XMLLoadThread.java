package Loader.Threads;

import Loader.*;
import Storages.Storage;
import Storages.Utilities.Utils;
import Storages.XMLStorage;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

public class XMLLoadThread extends Thread{

    public void run() {
        try {
            Date date = Utils.generateDate(XMLStorage.getThreadsWorks());
            XMLStorage.incThreadsWorks();
            URL url = new URL(XMLDownloader.getUrl().replaceAll("MYDATE", Storage.dateFormat.format(date)));
            XMLStorage.addPageToPool(Loader.urlToString(url));
            System.out.println("Page for "+Storage.dateFormat.format(date)+" - added.");
            XMLStorage.incThreadsCompletes();
        } catch (MalformedURLException me){
            System.out.println("Warning! The thread has not completed :  "+ me);
        }
    }
}
