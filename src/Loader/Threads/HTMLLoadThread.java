package Loader.Threads;

import Loader.HTMLDownloader;
import Loader.Loader;
import Storages.HTMLStorage;
import Storages.Storage;
import Storages.Utilities.Utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

public class HTMLLoadThread extends Thread{

    public void run() {
        try {
            Date date = Utils.generateDate(HTMLStorage.getThreadsWorks());
            HTMLStorage.incThreadsWorks();
            URL url = new URL(HTMLDownloader.getUrl().replaceAll("MYDATE", Storage.dateFormat.format(date)));
            HTMLStorage.addPageToPool(Loader.urlToString(url));
            System.out.println("Page for "+Storage.dateFormat.format(date)+" - added.");
            HTMLStorage.incThreadsCompleted();
        } catch (MalformedURLException me){
            System.out.println("Warning! The thread has not completed : " + me);
        }
    }
}
