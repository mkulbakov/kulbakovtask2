package Loader;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class Loader {

    public static void downloadPageAtFile(String fileName, URL u){
        try{
            String line;
            HttpURLConnection con = (HttpURLConnection) u.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            FileWriter fileWriter = new FileWriter(new File(fileName));
            while ((line = br.readLine()) != null) fileWriter.write(line);
            br.close();
        } catch (IOException ioe){
            System.out.println("Download failed : " + ioe);
        }
    }

    public static String urlToString(URL u){
        String line;
        String result = "";
        try{
            HttpURLConnection con = (HttpURLConnection) u.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            while ((line = br.readLine()) != null) result += line;
            br.close();
        } catch (IOException ioe){
            System.out.println("Download failed : " + ioe);
        }
        return result;
    }

}
