package Parsers;

import Storages.ValuteObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrParser extends Parser {

    private final static Pattern OBJECT = Pattern.compile("[{](.+?)[}]");
    private final static Pattern ABBREVIATION = Pattern.compile("\"Cur_Abbreviation\":\"(.+?)\"");
    private final static Pattern SCALE = Pattern.compile("\"Cur_Scale\":(.+?),");
    private final static Pattern RATE = Pattern.compile("\"Cur_OfficialRate\":(.+?)$");
    private final static Pattern DATE = Pattern.compile("\"Date\":\"(.+?)\"");

    private static ArrayList<String> objects;
    private static ArrayList<String> abbreviation;
    private static ArrayList<Integer> scale;
    private static ArrayList<Double> rate;
    private static String date;

    public static void parse(String s){
        objects = new ArrayList<String>();
        abbreviation = new ArrayList<String>();
        scale = new ArrayList<Integer>();
        rate = new ArrayList<Double>();
        date = new String();
        Matcher m = OBJECT.matcher(s);
        while (m.find()){
            objects.add(m.group(1));
        }
        for (String obj : objects){
            m = ABBREVIATION.matcher(obj);
            if (m.find())abbreviation.add(m.group(1));
            m = SCALE.matcher(obj);
            if (m.find())scale.add(Integer.parseInt(m.group(1)));
            m = RATE.matcher(obj);
            if (m.find())rate.add(Double.parseDouble(m.group(1)));
            m = DATE.matcher(obj);
            if (m.find())date = m.group(1).substring(0,10);
        }
    }

    public static ValuteObject generateValuteObject(String abb){
        int index = 0;
        while (!abbreviation.get(index).equals(abb)) index++;
        return new ValuteObject(abbreviation.get(index),scale.get(index),rate.get(index),date);
    }
}
