package Parsers;

import Storages.ValuteObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;

public class XMLParser extends Parser {

    private static ArrayList<String> objects;
    private static ArrayList<String> abbreviation;
    private static ArrayList<Integer> scale;
    private static ArrayList<Double> rate;
    private static String date;

    public static void parse(String xmlPage){
        try {
            abbreviation = new ArrayList<String>();
            scale = new ArrayList<Integer>();
            rate = new ArrayList<Double>();
            date = new String();

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = (Document) dBuilder.parse(new InputSource(new StringReader(xmlPage)));
            doc.getDocumentElement().normalize();
            date = doc.getDocumentElement().getAttribute("Date");
            NodeList nList = doc.getElementsByTagName("Currency");
            for (int i = 0; i< nList.getLength(); i++){
                Node node = nList.item(i);
                Element element = (Element) node;
                abbreviation.add(((Element) node).getElementsByTagName("CharCode").item(0).getTextContent());
                scale.add(Integer.parseInt(((Element) node).getElementsByTagName("Scale").item(0).getTextContent()));
                rate.add(Double.parseDouble(((Element) node).getElementsByTagName("Rate").item(0).getTextContent()));
            }
        } catch (Exception e){
            System.out.println(e);
        }
    }

    public static ValuteObject generateValuteObject(String abb){
        int index = 0;
        while (!abbreviation.get(index).equals(abb)) index++;
        return new ValuteObject(abbreviation.get(index),scale.get(index),rate.get(index),date);
    }

}
